
Description
-----------
This module imports and manages Skarabee publications trough the Skarabee
Weblink Service.

This module will create a new node content type, create fields for attributes
and download related images for all publications.

Requirements
------------
1) Subscription to the Skarabee Weblink service, see http://www.skarabee.com/
   for more information.

2) PHP SOAP extension to be enabled.

Installation
------------
1) Place this module directory in your "modules" folder (this will usually be
   "sites/all/modules/").

2) Enable the module.

3) Visit "admin/config/system/skarabee" and provide the credentials received
  from Skarabee.

4) Hit the "Update" button.
