<?php
/**
 * @file
 * Skarabee weblink methods.
 * Used for connection and fetching objects through the SOAP interface.
 */

/**
 * Get Soap client for the Skarabee webservice.
 *
 * @return object
 *   Returns SOAP client.
 */
function _skarabee_get_client() {
  global $_skarabee_client;

  // Get weblink variables.
  $wsdl = variable_get('skarabee_weblink_wsdl', '');
  $username = variable_get('skarabee_weblink_username', '');
  $password  = variable_get('skarabee_weblink_password', '');

  if (!$_skarabee_client instanceof stdClass) {
    if($wsdl && $username && $password) {
      try {
        $_skarabee_client = @new SoapClient($wsdl, array(
          'login' => $username,
          'password' => $password,
          'exceptions' => TRUE,
          'connection_timeout' => 10,
          'cache_wsdl' => WSDL_CACHE_NONE,
        ));
      } catch (SoapFault $e) {
        watchdog('skarabee', 'Couldn\'t establish connection to Skarabee weblink service. (' . $e->getMessage() . ')', NULL, WATCHDOG_ERROR);
        return FALSE;
      }
    } else {
      return FALSE;
    }
  }
  return $_skarabee_client;
}

/**
 * Retrieves all publications from the service.
 *
 * @return array
 *   Skarabee publication summaries.
 */
function _skarabee_get_publication_summaries() {
  $client = _skarabee_get_client();
  return $client->GetPublicationSummaries()->GetPublicationSummariesResult->PublicationSummaries->PublicationSummary;
}

/**
 * Senitizes a fieldname based on the category and fieldname.
 *
 * @param int $publication_id
 *   Publication id used in Skarabee.
 *
 * @return object
 *   Full Skarabee publication.
 */
function _skarabee_get_publication($publication_id) {
  $client = _skarabee_get_client();
  return $client->getPublication(array('PublicationId' => $publication_id))->GetPublicationResult->Publication;
}

/**
 * Find node by publication of create a new node.
 *
 * @param object $publication
 *   Skarabee publication.
 *
 * @return object
 *   New or existing node.
 */
function _skarabee_load_node($publication) {
  $query = new EntityFieldQuery();
  $result = $query
  ->entityCondition('entity_type', 'node')
  ->fieldCondition('info_id', 'value', $publication->Info->ID, '=')
  ->execute();

  if ($result) {
    $node = node_load(current($result['node'])->nid);
  }
  else {
    $node = new stdClass();
    $node->type = 'skarabee_publication';
    node_object_prepare($node);
  }

  return $node;
}

/**
 * Populates node with publication properties.
 *
 * @param object $node
 *   Node to be populated.
 *
 * @param object $publication
 *   Skarabee publication object.
 *
 * @return object
 *   Populated node.
 */
function _skarabee_node_set_properties($node, $publication) {
  // Create title based on publication.
  $title = $publication->Property->Address->Street;
  if (isset($publication->Property->Address->HouseNumber)) {
    $title .= ' ' . $publication->Property->Address->HouseNumber;
  }
  if (isset($publication->Property->Address->HouseNumberExtension)) {
    $title .= $publication->Property->Address->HouseNumberExtension;
  }
  if (isset($publication->Property->Address->City)) {
    $title .= ', ' . $publication->Property->Address->City->_;
  }
  $node->title = $title;
  $node->uid = 1;

  // Set default language.
  $node->language = LANGUAGE_NONE;

  // Set all fields available in this publication.
  foreach ($publication as $category => $attributes) {
    foreach ($attributes as $attribute => $value) {
      $field_id = _skarabee_sanitize_fieldname($category, $attribute);
      if ($category == 'Pictures') {
        if (is_array($value)) {
          foreach ($value as $key => $image) {

            $directory = 'public://skarabee/' . $publication->Info->ID;
            if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
              $filename = $image->Name;
              $filepath = $directory . '/' . $filename;
              if (!file_exists($filepath)) {
                $image_data = file_get_contents($image->URL);
                $file = file_save_data($image_data, $filepath, FILE_EXISTS_ERROR);
                if ($file) {
                  $node->{$field_id}[$node->language][$key]['fid'] = $file->fid;
                }
              }
            }
            else {
              watchdog('skarabee', 'directory %path cannot be created.', array('%path' => $directory), WATCHDOG_ERROR);
            }
          }
        }
      }
      else {
        if ($value == 'UNDEFINED') {
          $value = 0;
        }
        elseif ($value == 'FALSE') {
          $value = 0;
        }
        elseif ($value == 'TRUE') {
          $value = 1;
        }

        if (gettype($value) == 'boolean') {
          $value = (int) $value;
        }

        if (gettype($value) == 'object') {
          $value = serialize($value);
        }
        $node->{$field_id}[$node->language][0]['value'] = $value;
      }
    }
  }
  return $node;
}
