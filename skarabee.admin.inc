<?php
/**
 * @file
 * Administration pages and forms.
 *
 * This file controls all admin related pages in Drupal.
 */

/**
 * Provides the system settings form for the admin page.
 */
function skarabee_admin_settings() {

  // Load weblink file.
  module_load_include('weblink.inc', 'skarabee');
  if(_skarabee_get_client()) {
    drupal_set_message(t('Your site has contacted Skarabee Weblink server.'));
  } else {
    drupal_set_message(t('Couldn\'t establish connection to Skarabee weblink service.'), 'error');
  }

  $form['global'] = array(
    '#type' => 'fieldset',
    '#title' => t('Skarabee settings'),
    '#tree' => FALSE,
  );

  $form['global']['skarabee_weblink_wsdl'] = array(
    '#type' => 'textfield',
    '#title' => t('Weblink URL'),
    '#default_value' =>  variable_get('skarabee_weblink_wsdl', ''),
    '#description' => t('Example: http://weblink.skarabee.com/weblink.asmx?WSDL'),
    '#required' => TRUE,
  );

  $form['global']['skarabee_weblink_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Weblink Username'),
    '#default_value' =>  variable_get('skarabee_weblink_username', ''),
    '#description' => t('The username is provided by Skarabee.'),
    '#required' => TRUE,
  );

  $form['global']['skarabee_weblink_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Weblink Password'),
    '#default_value' =>  variable_get('skarabee_weblink_password', ''),
    '#description' => t('The password is provided by Skarabee.'),
    '#required' => TRUE,
  );

  $form['update'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update publications'),
    '#tree' => FALSE,
  );

  $form['update']['update'] = array(
    '#type' => 'submit',
    '#default_value' => t('Update'),
    '#submit' => array('skarabee_update_submit'),
  );

  $form['update']['delete'] = array(
    '#type' => 'submit',
    '#default_value' => t('Delete all'),
    '#submit' => array('skarabee_delete_submit'),
  );

  return system_settings_form($form);
}
